#include <Arduino.h>
#include "FastLED.h"

#define NUM_LEDS 6
#define LED_PIN 2
#define COLOR_ORDER GRB
CRGB LedStrip[NUM_LEDS];

// Function prototypes
void setLedStripColor(uint8_t R, uint8_t G, uint8_t B);

void setup() {
  Serial.begin(115200);
  FastLED.addLeds<WS2812B, LED_PIN, COLOR_ORDER>(LedStrip, NUM_LEDS);
}

void loop() {
  for(uint8_t G = 0; G < 256; G++) {
    setLedStripColor(20,G,20);
    delay(100);
  }
}

void setLedStripColor(uint8_t R, uint8_t G, uint8_t B)
{
  for(uint8_t ledIdx = 0; ledIdx < NUM_LEDS; ledIdx++) {
    LedStrip[ledIdx].setRGB(R, G, B);
  }
  FastLED.show();
}