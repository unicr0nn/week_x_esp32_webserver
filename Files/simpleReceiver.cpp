#include <Arduino.h>
#include "FastLED.h"

//Include for Webserver
#include <WiFi.h>
#include <HTTPClient.h>
#include "ESPAsyncWebServer.h"

// Fors RGB Ledstrip
#define NUM_LEDS 6
#define LED_PIN 2
#define COLOR_ORDER GRB
CRGB LedStrip[NUM_LEDS];

// Server settings
String html;
AsyncWebServer server(80);
const char* ssid = "RgbLedServer";
IPAddress local_ip(192,168,1,1);
IPAddress gateway(192,168,1,1);
IPAddress subnet(255,255,255,0);

// RGB Colors set by the Webserver:
uint8_t Red   = 0;
uint8_t Green = 0;
uint8_t Blue  = 0;

// Function prototypes
void setLedStripColor(uint8_t R, uint8_t G, uint8_t B);

// Server
void setupRgbLedServer(void);
void setupRgbLedHtmlString(void);
void setupRgbLedAP(void);

void setup() {
  Serial.begin(115200);
  FastLED.addLeds<WS2812B, LED_PIN, COLOR_ORDER>(LedStrip, NUM_LEDS);
  
  // Setup Server
  setupRgbLedHtmlString();
  setupRgbLedAP();
  setupRgbLedServer();
}

void loop() {
  for(uint8_t G = 0; G < 256; G++) {
    setLedStripColor(20,G,20);
    delay(100);
  }
}

void setupRgbLedHtmlString(void)
{
  html += "<!DOCTYPE html>\n";
  html += "<html lang=\"en\">\n";
  html += "<head>\n";
  html += "<meta charset=\"UTF-8\">\n";
  html += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n";
  html += "<meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n";
  html += "<title>Document</title>\n";
  html += "</head>\n";
  html += "<body> \n";
  html += "<div id=\"main_wrapper\"> \n";
  html += "<div id=\"main_header\"> <h1># RGB LED STRIP #</h1>\n";
  html += "<input id=\"submit_button\" type=\"button\" value=\"Click Button\" onclick=\"buttonClicked()\">\n";
  html += "<script>\'use strict\';\n";
  html += "function buttonClicked(){\n";
  html += "console.log(\"Button Clicked\");\n";
  html += "POST(\"HELLO\");}\n";
  html += "function POST(value) {\n";
  html += "let xmlHttp = new XMLHttpRequest();\n";
  html += "xmlHttp.onreadystatechange = () => {\n";
  html += "if (xmlHttp.readyState == 4 && xmlHttp.status == 200)\n";
  html += "console.log(\"Post done\");};\n";
  html += "xmlHttp.open(\"POST\", window.location.origin, true);\n";
  html += "xmlHttp.send(value);};\n";
  html += "</script>\n";
  html += "</body>\n";
  html += "</html>\n";

  Serial.println(html);
}

void setupRgbLedServer() {
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/html", html);
  });

    // In any other case fall back to 404
  server.onNotFound([](AsyncWebServerRequest *request) {
    request->send(404, "text/plain", "404 - Not Found");
  });

  server.onRequestBody([](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total){
    String recData;
    for(uint8_t i = 0; i < len; i++) {
      recData += ((char)data[i]);
    }
    Serial.print("POST Request arrived: ");
    Serial.println(recData);
  });

  // Listen and serve
  server.begin();
  Serial.println("RGB LED Server is listening on PORT 80");
}

void setLedStripColor(uint8_t R, uint8_t G, uint8_t B)
{
  for(uint8_t ledIdx = 0; ledIdx < NUM_LEDS; ledIdx++) {
    LedStrip[ledIdx].setRGB(R, G, B);
  }
  FastLED.show();
}

void setupRgbLedAP(void)
{
  WiFi.persistent(false);
  WiFi.softAP(ssid, NULL);
  delay(100);
  WiFi.softAPConfig(local_ip, gateway, subnet);
  delay(100);
  Serial.println("Wifi AP has been set up");
  Serial.print("SSID: ");
  Serial.println(ssid);
  Serial.println("IP  : 192.168.1.1\n");
}