#include <Arduino.h>
#include "FastLED.h"

//Include for Webserver
#include <WiFi.h>
#include <HTTPClient.h>
#include "ESPAsyncWebServer.h"

// Fors RGB Ledstrip
#define NUM_LEDS 6
#define LED_PIN 2
#define COLOR_ORDER GRB
CRGB LedStrip[NUM_LEDS];

// Server settings
String html;
AsyncWebServer server(80);
const char* ssid = "RgbLedServer";
IPAddress local_ip(192,168,1,1);
IPAddress gateway(192,168,1,1);
IPAddress subnet(255,255,255,0);

// Function prototypes
void setLedStripColor(uint8_t R, uint8_t G, uint8_t B);

// Server
void setupRgbLedServer(void);
void setupRgbLedHtmlString(void);
void setupRgbLedAP(void);

void setup() {
  Serial.begin(115200);
  FastLED.addLeds<WS2812B, LED_PIN, COLOR_ORDER>(LedStrip, NUM_LEDS);
  
  // Setup Server
  setupRgbLedHtmlString();
  setupRgbLedAP();
  setupRgbLedServer();
}

void loop() {
  for(uint8_t G = 0; G < 256; G++) {
    setLedStripColor(20,G,20);
    delay(100);
  }
}

void setupRgbLedHtmlString(void)
{
  html += "<!DOCTYPE html>\n";
  html += "<html>\n";
  html += "<body>\n";
  html += "<h1>LED SERVER</h1>\n";
  html += "<p>FIRST TEST</p>\n";
  html += "</body>\n";
  html += "</html>\n";
  Serial.println(html);
}

void setupRgbLedServer() {
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/html", html);
  });

    // In any other case fall back to 404
  server.onNotFound([](AsyncWebServerRequest *request) {
    request->send(404, "text/plain", "404 - Not Found");
  });

  // Listen and serve
  server.begin();
  Serial.println("RGB LED Server is listening on PORT 80");
}

void setupRgbLedAP(void)
{
  WiFi.persistent(false);
  WiFi.softAP(ssid, NULL);
  delay(100);
  WiFi.softAPConfig(local_ip, gateway, subnet);
  delay(100);
  Serial.println("Wifi AP has been set up");
  Serial.print("SSID: ");
  Serial.println(ssid);
  Serial.println("IP  : 192.168.1.1\n");
}

void setLedStripColor(uint8_t R, uint8_t G, uint8_t B)
{
  for(uint8_t ledIdx = 0; ledIdx < NUM_LEDS; ledIdx++) {
    LedStrip[ledIdx].setRGB(R, G, B);
  }
  FastLED.show();
}